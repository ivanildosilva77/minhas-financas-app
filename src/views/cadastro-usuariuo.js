import React from "react";
import Card from '../components/card';
import FormGroup from '../components/form-group';

class CadastroUsuario extends React.Component {

    state = {
        nome: '',
        email: '',
        senha: '',
        senhaRepeticao: ''
    }

    cadastrar = () =>{
        console.log(this.state)
    }

    render() {
        return (
            <div className="container">
                <Card title="Cadastro de Usuário">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="bs-component">
                                <FormGroup label="Nome: *" htmlFor="txtNome">
                                    <input type="text"
                                        className="form-control"
                                        id="txtNome"
                                        name="nome"
                                        value={this.state.nome}
                                        onChange={e => this.setState({ nome: e.target.value })} />
                                </FormGroup>
                                <FormGroup label="Email: *" htmlFor="txtEmail">
                                    <input type="text"
                                        className="form-control"
                                        id="txtEmail"
                                        name="email"
                                        value={this.state.email}
                                        onChange={e => this.setState({ email: e.target.value })} />
                                </FormGroup>
                                <FormGroup label="Senha: *" htmlFor="txtSenha">
                                    <input type="password"
                                        className="form-control"
                                        id="txtSenha"
                                        name="senha"
                                        value={this.state.senha}
                                        onChange={e => this.setState({ senha: e.target.value })} />
                                </FormGroup>
                                <FormGroup label="Repita a senha: *" htmlFor="txtRepite">
                                    <input type="password"
                                        className="form-control"
                                        id="txtRepite"
                                        name="senhaRepeticao"
                                        value={this.state.senhaRepeticao}
                                        onChange={e => this.setState({ senhaRepeticao: e.target.value })} />
                                </FormGroup>
                                <button onClick={this.cadastrar} className="btn btn-success">Entrar</button>
                                <button className="btn btn-danger" >Cadastrar</button>
                            </div>
                        </div>
                    </div>
                </Card>
            </div>
        )
    }
}

export default CadastroUsuario